//
//  Workshop.cpp
//  Workshop
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "Workshop.h"

#include <Engine/EngineCommon.hpp>
#include <glm/gtx/quaternion.hpp>
#include <typeinfo>
#include <Engine/CommandSystem.h>
#include <Engine/VariableSystem.h>
#include <Engine/Render/RenderSystem.h>
#include <Engine/Render/RenderConfig.h>
#include <Engine/Render/RenderWorldManager.h>
#include <Engine/FileSystem.h>
#include <Engine/InputManager.h>
#include <Engine/Scenegraph/Transform.h>
namespace tin {
    
    Workshop::~Workshop(){
        
    }
    
    void Workshop::Init(){
        
    }
    
    void Workshop::Start(){
        tin::VariableSystem::SharedInstance()->Set("g_flightcam", false);
        tin::RenderWorldManager::SharedInstance()->LoadWorld("Arrows.tinScene");
        started = true;
        viewport_name = "forward";
        RenderConfig config = RenderConfig::LoadFromFile("lineDebug.tinRenderConfig");
        RenderSystem::SharedInstance()->SetRenderConfig(config);
        
        Ray x_axis = Ray(glm::vec3(-10.0, 0.0, 0.0), glm::vec3(1.0, 0.0, 0.0), 20.0, glm::vec4(1.0, 0.0, 0.0, 1.0));
        Ray y_axis = Ray(glm::vec3(0.0, -10.0, 0.0), glm::vec3(0.0, 1.0, 0.0), 20.0, glm::vec4(0.0, 1.0, 0.0, 1.0));
        Ray z_axis = Ray(glm::vec3(0.0, 0.0, -10.0), glm::vec3(0.0, 0.0, 1.0), 20.0, glm::vec4(0.0, 0.0, 1.0, 1.0));
        RenderSystem::SharedInstance()->AddDebugRay(x_axis);
        RenderSystem::SharedInstance()->AddDebugRay(y_axis);
        RenderSystem::SharedInstance()->AddDebugRay(z_axis);
    }
    
    bool Workshop::isStarted(){
        return started;
    }
    
    void Workshop::Shutdown(){
        
    }
    
    std::vector<std::string> Workshop::SceneFilenames(){
        return tin::FileSystem::SharedInstance()->ListFilesOfType("tinScene");
    }
    
    void Workshop::AddVirtualFileSystem(std::string file_name){
        tin::FileSystem::SharedInstance()->AddFileSystem(file_name, tin::VFSTypeArchive);
    }
    
    void Workshop::AddVirtualFilePath(std::string path){
        tin::FileSystem::SharedInstance()->AddFileSystem(path, tin::VFSTypePath);
    }
    
    void Workshop::UpdateCamera(Camera *camera, RenderWorld *world){
        tin::InputMouse mouse = InputManager::SharedInstance()->Mouse();

        Transform transform = world->GetTransform(camera->TransformId());
        glm::quat rotX = glm::angleAxis(-mouse.delta.y , glm::vec3(1.0f, 0.0f, 0.0f));
        glm::quat rotY = glm::angleAxis(-mouse.delta.x , glm::vec3(0.0f, 1.0f, 0.0f));
        glm::quat new_rotation = rotY * transform.rotation();
        
        float move_speed = 0.1f;
        glm::vec3 movement(0);
        
        if (InputManager::SharedInstance()->IsButtonDown(KEY_W)){
            movement.z = -move_speed;
        }
        if (InputManager::SharedInstance()->IsButtonDown(KEY_S)){
            movement.z = move_speed;
        }
        if (InputManager::SharedInstance()->IsButtonDown(KEY_A)){
            movement.x = -move_speed;
        }
        if (InputManager::SharedInstance()->IsButtonDown(KEY_D)){
            movement.x = move_speed;
        }
        if (InputManager::SharedInstance()->IsButtonDown(KEY_ESC)){
            ExitInteractiveMode();
        }
        
        movement = new_rotation * movement;
        glm::vec3 new_position = movement + transform.position();
        transform.position(new_position);
        
        transform.rotation(new_rotation * rotX);
        world->SetTransform(camera->TransformId(), transform);
    }
    
    void Workshop::Frame(){;
        // Current world?
        RenderWorld *world = RenderWorldManager::SharedInstance()->GetWorld(current_world_);
        if (world){
            
            Camera *camera = world->GetCamera(0);
            if (camera){
                if (VariableSystem::SharedInstance()->GetAsBool("g_flightcam")){
                    UpdateCamera(camera, world);
                } else {
                    if (InputManager::SharedInstance()->ButtonUp(MOUSE_0)){
                        tin::InputMouse mouse = InputManager::SharedInstance()->Mouse();
                        Ray ray = world->ScreenPointToWorldRay(mouse.position, camera);
                        RenderSystem::SharedInstance()->AddDebugRay(ray);
                    }
                }
                
                if (!world->LoadInProgress()){
                    RenderWorldManager::SharedInstance()->DrawWorld(camera, world, viewport_name);
                }
            }
        }        
    }
    
    void Workshop::LoadScene(std::string filename){
        Json::Value value;
        value["filename"] = filename;
        CommandSystem::SharedInstance()->QueueCommand("rwm_LoadWorld", value);
        current_world_ = filename;
    }
    
    void Workshop::CloseScene(std::string filename){
        Json::Value value;
        value["filename"] = filename;
        CommandSystem::SharedInstance()->QueueCommand("rwm_UnloadWorld", value);
    }
    
    void Workshop::SetEnableFlightCam(bool enable){
        VariableSystem::SharedInstance()->Set("g_flightcam", enable);
    }
    
    void Workshop::SetCurrentWorld(std::string world_name){
        
    }
}