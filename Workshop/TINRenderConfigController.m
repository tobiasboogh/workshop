//
//  TINRenderConfigController.m
//  SceneViewer
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINRenderConfigController.h"
#include <list>
#include <string>
#include <Engine/Render/RenderConfig.h>
#include <Engine/Render/RenderSystem.h>
#include <Engine/JsonParser.h>

@interface TINRenderConfigController(){
    tin::RenderConfig       config;
    std::list<std::string>  modifiers;
}
@end

@implementation TINRenderConfigController

-(void)awakeFromNib{
    [self setLayerControlsToEnabled:NO];
    config = tin::RenderSystem::SharedInstance()->CurrentRenderConfig();
    [self refreshValues];
}

-(void)refreshValues{
    modifiers = tin::RenderSystem::SharedInstance()->ModifierNames();
    modifiers.push_front("None");
    [self.viewportTableView reloadData];
    [self.globalRenderTargetsTableView reloadData];
}

-(IBAction)updateRenderConfig:(id)sender{
    Json::Value json_config = tin::JSONParser::EncodeRenderConfig(config);
    tin::RenderSystem::SharedInstance()->rs_SetRenderConfig(json_config);
}

-(void)refreshTargetView{
    [self.globalRenderTargetsTableView reloadData];
}

-(void)tabView:(NSTabView *)tabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem{
    if ([tabView indexOfTabViewItem:tabViewItem] == 1){
        [self refreshTargetView];
    }
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    if (tableView == self.viewportTableView){
        return config.NumViewports();
    } else if (tableView == self.layerTableView){
        if (self.viewportTableView.selectedRow == -1){
            return 0;
        }
        tin::Viewport viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        return viewport.NumLayers();
    } else if (tableView == self.layerRendertargetsTableView){
        if (self.viewportTableView.selectedRow == -1){
            return 0;
        }
        if (self.layerTableView.selectedRow == -1){
            return 0;
        }
        tin::Viewport viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        return layer.render_targets.size();
    } else if (tableView == self.layerInputsTableView){
        if (self.viewportTableView.selectedRow == -1){
            return 0;
        }
        if (self.layerTableView.selectedRow == -1){
            return 0;
        }
        tin::Viewport viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        return layer.inputs.size();
    } else if (tableView == self.globalRenderTargetsTableView){
        return config.NumRenderTargets();
    }
    return 0;
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    if (tableView == self.viewportTableView){
        return [NSString stringWithFormat:@"%s", config.GetViewport((int32_t)row).name().c_str()];
    } else if (tableView == self.layerTableView){
        if (self.viewportTableView.selectedRow == -1){
            return nil;
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)row, layer);
        return [NSString stringWithFormat:@"%s", layer.name.c_str()];
    } else if (tableView == self.globalRenderTargetsTableView){
        if ([tableColumn.identifier isEqual: @"name"]){
            return [NSString stringWithFormat:@"%s", config.GetRenderTarget((int32_t)row).name.c_str()];
        }
    }
    return nil;
}

-(void)tableView:(NSTableView *)tableView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    if (tableView == self.layerRendertargetsTableView) {
        if (self.layerTableView.selectedRow == -1){
            return;
        }
        if (self.viewportTableView.selectedRow == -1){
            return;
        }
        
        NSPopUpButtonCell *popUpCell = (NSPopUpButtonCell *)cell;
        [popUpCell removeAllItems];
        for (int i=0; i < config.NumRenderTargets(); i++){
            [popUpCell addItemWithTitle:[NSString stringWithFormat:@"%s", config.GetRenderTarget(i).name.c_str()]];
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        if (layer.render_targets.size() > 0){
            for (int i=0; i < config.NumRenderTargets(); i++){
                if (config.GetRenderTarget(i).name == layer.render_targets[row]){
                    [popUpCell selectItemAtIndex:i];
                    break;
                }
            }
        }
    } else if (tableView == self.layerInputsTableView){
        if (self.layerTableView.selectedRow == -1){
            return;
        }
        if (self.viewportTableView.selectedRow == -1){
            return;
        }
        
        NSPopUpButtonCell *popUpCell = (NSPopUpButtonCell *)cell;
        [popUpCell removeAllItems];
        for (int i=0; i < config.NumRenderTargets(); i++){
            [popUpCell addItemWithTitle:[NSString stringWithFormat:@"%s", config.GetRenderTarget(i).name.c_str()]];
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        if (layer.inputs.size() > 0){
            for (int i=0; i < config.NumRenderTargets(); i++){
                if (config.GetRenderTarget(i).name == layer.inputs[row]){
                    [popUpCell selectItemAtIndex:i];
                    break;
                }
            }
        }
    } else if (tableView == self.globalRenderTargetsTableView) {
        if ([tableColumn.identifier isEqualTo:@"format"]){
            NSPopUpButtonCell *popUpCell = (NSPopUpButtonCell *)cell;
            [popUpCell removeAllItems];
            [popUpCell addItemWithTitle:@"ALPHA"];
            [popUpCell addItemWithTitle:@"RGB"];
            [popUpCell addItemWithTitle:@"RGBA"];
            [popUpCell addItemWithTitle:@"LUMINANCE"];
            [popUpCell addItemWithTitle:@"LUMINANCE_ALPHA"];
            [popUpCell addItemWithTitle:@"DEPTH24_STENCIL8"];
            [popUpCell addItemWithTitle:@"RGB16"];
            [popUpCell selectItemAtIndex:config.GetRenderTarget((int32_t)row).format];
        }
    }
}

-(void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    if (tableView == self.viewportTableView){
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        viewport.name([object cStringUsingEncoding:NSUTF8StringEncoding]);
        config.SetViewport((int32_t)row, viewport);
    } else if (tableView == self.layerTableView){
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        layer.name = [object cStringUsingEncoding:NSUTF8StringEncoding];
        viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
        config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    } else if (tableView == self.layerRendertargetsTableView){
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        int selection = [object intValue];
        layer.render_targets[row] = config.GetRenderTarget(selection).name;
        viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
        config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    } else if (tableView == self.layerInputsTableView){
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        int selection = [object intValue];
        layer.inputs[row] = config.GetRenderTarget(selection).name;
        viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
        config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    } else if (tableView == self.globalRenderTargetsTableView){
        if ([tableColumn.identifier isEqualTo:@"name"]){
            auto target = config.GetRenderTarget((int32_t)row);
            target.name = [object cStringUsingEncoding:NSUTF8StringEncoding];
            config.SetRenderTarget((int32_t)row, target);
        } else if ([tableColumn.identifier isEqualTo:@"format"]){
            auto target = config.GetRenderTarget((int32_t)row);
            target.format = (tin::RenderTargetFormat)[object intValue];
            config.SetRenderTarget((int32_t)row, target);
        }
    }
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSTableView *tableView = notification.object;
    if (tableView == self.viewportTableView){
        if (tableView.selectedRow == -1){
            [self setLayerControlsToEnabled:NO];
            [self.layerTableView reloadData];
            return;
        }
        auto viewport = config.GetViewport((int32_t)tableView.selectedRow);
        if (viewport.NumLayers() > 0){
            [self.layerTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
            [self.layerTableView reloadData];
            [self setLayerControlsToEnabled:YES];
            [self.layerRendertargetsTableView reloadData];
        } else {
            [self setLayerControlsToEnabled:NO];
        }
    } else if (tableView == self.layerTableView) {
        if (self.viewportTableView.selectedRow == -1){
            return;
        }
        if (tableView.selectedRow == -1){
            [self setLayerControlsToEnabled:NO];
            return;
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        self.clearCheckbox.state = layer.clear;
        self.writedepthCheckbox.state = layer.write_depth;
        self.writecolorCheckbox.state = layer.write_color;
        NSMutableArray *modiferArray = [[NSMutableArray alloc] init];
        for (auto modifier : modifiers){
            [modiferArray addObject:[NSString stringWithFormat:@"%s", modifier.c_str()]];
        }
        [self.modifierPopupButton removeAllItems];
        [self.modifierPopupButton addItemsWithTitles:modiferArray];
        if (layer.modifier == ""){
            [self.modifierPopupButton selectItemAtIndex:0];
        } else {
            [self.modifierPopupButton selectItemWithTitle:[NSString stringWithFormat:@"%s", layer.modifier.c_str()]];
        }
        
        [self.layerRendertargetsTableView reloadData];
        [self.layerInputsTableView reloadData];
        
        [self.depthTargetPopupButton removeAllItems];
        for (int i=0; i < config.NumRenderTargets(); i++){
            [self.depthTargetPopupButton addItemWithTitle:[NSString stringWithFormat:@"%s", config.GetRenderTarget(i).name.c_str()]];
            if (config.GetRenderTarget(i).name == layer.depth_stencil_target){
                [self.depthTargetPopupButton selectItemAtIndex:i];
            }
        }
        [self setLayerControlsToEnabled:YES];
    }
}

-(void)setLayerControlsToEnabled:(BOOL)enabled{
    [self.clearCheckbox setEnabled:enabled];
    [self.writedepthCheckbox setEnabled:enabled];
    [self.writecolorCheckbox setEnabled:enabled];
    [self.depthTargetPopupButton setEnabled:enabled];
    [self.modifierPopupButton setEnabled:enabled];
    [self.layerRendertargetsTableView reloadData];
    
}

-(IBAction)addViewport:(id)sender{
    tin::Viewport viewport;
    viewport.name("New viewport");
    config.AddViewport(viewport);
    [self.viewportTableView reloadData];
}

-(IBAction)removeViewport:(id)sender{
    int64_t row = [self.viewportTableView selectedRow];
    if (row == -1){
        return;
    }
    [self.viewportTableView reloadData];
}

-(IBAction)addLayer:(id)sender{
    int64_t viewport_row = [self.viewportTableView selectedRow];
    if (viewport_row == -1){
        return;
    }
    auto viewport = config.GetViewport((int32_t)viewport_row);
    tin::RenderLayer layer;
    layer.name = "New layer";
    viewport.AddLayer(layer);
    config.SetViewport((int32_t)viewport_row, viewport);
    [self.layerTableView reloadData];
}

-(IBAction)removeLayer:(id)sender{
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    int64_t viewport_row = [self.viewportTableView selectedRow];
    int64_t layer_row = [self.layerTableView selectedRow];
    auto viewport = config.GetViewport((int32_t)viewport_row);
    viewport.RemoveLayer((int32_t)layer_row);
    config.SetViewport((int32_t)viewport_row, viewport);
    [self.layerTableView reloadData];
}

-(IBAction)moveLayerUp:(id)sender{
    int64_t viewport_row = [self.viewportTableView selectedRow];
    int32_t layer_row = (int32_t)[self.layerTableView selectedRow];
    if (layer_row == 0){
        return;
    }
    auto viewport = config.GetViewport((int32_t)viewport_row);
    tin::RenderLayer layer;
    
    viewport.GetLayer(layer_row, layer);
    viewport.RemoveLayer(layer_row);
    viewport.InsertLayer(layer_row - 1, layer);
    
    config.SetViewport((int32_t)viewport_row, viewport);
    [self.layerTableView reloadData];
    [self.layerTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:layer_row - 1] byExtendingSelection:NO];
}

-(IBAction)moveLayerDown:(id)sender{
    int64_t viewport_row = [self.viewportTableView selectedRow];
    int32_t layer_row = (int32_t)[self.layerTableView selectedRow];
    auto viewport = config.GetViewport((int32_t)viewport_row);

    if (layer_row == viewport.NumLayers()-1){
        return;
    }
    
    tin::RenderLayer layer;
    viewport.GetLayer(layer_row, layer);
    viewport.RemoveLayer(layer_row);
    viewport.InsertLayer(layer_row+1, layer);
    
    config.SetViewport((int32_t)viewport_row, viewport);
    [self.layerTableView reloadData];
    [self.layerTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:layer_row + 1] byExtendingSelection:NO];
}

-(IBAction)addRenderTarget:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }

    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);

    layer.render_targets.push_back(config.GetRenderTarget(0).name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerRendertargetsTableView reloadData];
}

-(IBAction)removeRenderTarget:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    if (self.layerRendertargetsTableView.selectedRow == -1){
        return;
    }
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    auto iter = layer.render_targets.begin() + self.layerRendertargetsTableView.selectedRow;
    layer.render_targets.erase(iter);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    
    [self.layerRendertargetsTableView reloadData];
}

-(IBAction)moveRenderTargetDown:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    int64_t row = self.layerRendertargetsTableView.selectedRow;
    if (row == layer.render_targets.size()-1){
        return;
    }
    auto iter = layer.render_targets.begin() + row;
    std::string target_name = layer.render_targets[row];
    layer.render_targets.erase(iter);
    layer.render_targets.insert(iter+1, target_name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerRendertargetsTableView reloadData];
    [self.layerRendertargetsTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row+1] byExtendingSelection:NO];
}

-(IBAction)moveRenderTargetUp:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    int64_t row = self.layerRendertargetsTableView.selectedRow;
    if (row <= 0){
        return;
    }
    
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    
    auto iter = layer.render_targets.begin() + row;
    std::string target_name = layer.render_targets[row];
    layer.render_targets.erase(iter);
    layer.render_targets.insert(iter-1, target_name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerRendertargetsTableView reloadData];
    [self.layerRendertargetsTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row-1] byExtendingSelection:NO];
}

-(IBAction)addInput:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    
    layer.inputs.push_back(config.GetRenderTarget(0).name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerInputsTableView reloadData];
}

-(IBAction)removeInput:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    if (self.layerInputsTableView.selectedRow == -1){
        return;
    }
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    auto iter = layer.inputs.begin() + self.layerInputsTableView.selectedRow;
    layer.inputs.erase(iter);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    
    [self.layerInputsTableView reloadData];
}

-(IBAction)moveInputUp:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    int64_t row = self.layerInputsTableView.selectedRow;
    if (row <= 0){
        return;
    }
    
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    
    auto iter = layer.inputs.begin() + row;
    std::string target_name = layer.inputs[row];
    layer.inputs.erase(iter);
    layer.inputs.insert(iter-1, target_name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerInputsTableView reloadData];
    [self.layerInputsTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row-1] byExtendingSelection:NO];
}

-(IBAction)moveInputDown:(id)sender{
    if (self.layerTableView.selectedRow == -1){
        return;
    }
    if (self.viewportTableView.selectedRow == -1){
        return;
    }
    
    auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    
    int64_t row = self.layerInputsTableView.selectedRow;
    if (row == layer.inputs.size()-1){
        return;
    }
    auto iter = layer.inputs.begin() + row;
    std::string target_name = layer.inputs[row];
    layer.inputs.erase(iter);
    layer.inputs.insert(iter+1, target_name);
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    [self.layerInputsTableView reloadData];
    [self.layerInputsTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row+1] byExtendingSelection:NO];
}

-(IBAction)checkboxChanged:(id)sender{
    int64_t viewport_row = [self.viewportTableView selectedRow];
    int64_t layer_row = [self.layerTableView selectedRow];
    if (viewport_row == -1 || layer_row == -1){
        return;
    }
    auto viewport = config.GetViewport((int32_t)viewport_row);
    
    tin::RenderLayer layer;
    viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
    
    if (sender == self.clearCheckbox){
        layer.clear = [sender state];
    } else if (sender == self.writecolorCheckbox) {
        layer.write_color = [sender state];
    } else if (sender == self.writedepthCheckbox){
        layer.write_depth = [sender state];
    }
    
    viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
    config.SetViewport((int32_t)viewport_row, viewport);
}

-(IBAction)popUpChanged:(id)sender{
    if ([sender isEqual:self.depthTargetPopupButton]){
        if (self.layerTableView.selectedRow == -1){
            return;
        }
        if (self.viewportTableView.selectedRow == -1){
            return;
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        
        layer.depth_stencil_target = [self.depthTargetPopupButton.selectedItem.title cStringUsingEncoding:NSUTF8StringEncoding];
        viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
        config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    } else if ([sender isEqual:self.modifierPopupButton]){
        if (self.layerTableView.selectedRow == -1){
            return;
        }
        if (self.viewportTableView.selectedRow == -1){
            return;
        }
        auto viewport = config.GetViewport((int32_t)self.viewportTableView.selectedRow);
        tin::RenderLayer layer;
        viewport.GetLayer((int32_t)self.layerTableView.selectedRow, layer);
        layer.modifier = [self.modifierPopupButton.selectedItem.title cStringUsingEncoding:NSUTF8StringEncoding];
        viewport.SetLayer((int32_t)self.layerTableView.selectedRow, layer);
        config.SetViewport((int32_t)self.viewportTableView.selectedRow, viewport);
    }
}

-(IBAction)saveRenderConfig:(id)sender{
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setCanCreateDirectories:NO];
    [savePanel setAllowedFileTypes:@[@"tinRenderConfig"]];
    [savePanel runModal];
    [savePanel URL];
    Json::Value json = tin::JSONParser::EncodeRenderConfig(config);
    std::string json_string = tin::JSONParser::JSONToString(json);
    NSString *nsString = [NSString stringWithCString:json_string.c_str() encoding:NSUTF8StringEncoding];
    [[nsString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:[[savePanel URL] path] atomically:YES];
}

-(IBAction)openRenderConifg:(id)sender{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseFiles:YES];
    [openPanel setCanCreateDirectories:NO];
    [openPanel setAllowedFileTypes:@[@"tinRenderConfig"]];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel runModal];
    if ([[openPanel URLs] count] > 0){
        NSURL *url = [[openPanel URLs] objectAtIndex:0];
        NSString *string = [NSString stringWithContentsOfFile:[url path] encoding:NSUTF8StringEncoding error:nil];
        if (string != nil){
            std::string string_config = [string cStringUsingEncoding:NSUTF8StringEncoding];
            Json::Value json = tin::JSONParser::StringToJson(string_config);
            tin::RenderConfig render_config = tin::JSONParser::JsonToRenderConfig(json);
            config = render_config;
            [self refreshValues];
        }
    }
}

-(IBAction)addGlobalRenderTarget:(id)sender{
    tin::RenderTarget target;
    target.name = "New target";
    config.AddRendertarget(target);
    [self.globalRenderTargetsTableView reloadData];
}

-(IBAction)removeGlobalRenderTarget:(id)sender{
    int64_t row = [self.globalRenderTargetsTableView selectedRow];
    config.RemoveRenderTarget(row);
    [self.globalRenderTargetsTableView reloadData];
}
@end
