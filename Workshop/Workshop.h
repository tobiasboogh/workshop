//
//  Workshop.h
//  SceneViewer
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _Workshop_H_
#define _Workshop_H_
#include <Engine/Application.h>
#include <string>
#include <vector>
namespace tin{
    class Camera;
    class RenderWorld;
    class Variable;
    class Workshop : public Application{
        public :
        ~Workshop();
        void Init();
        void Start();
        void Shutdown();
        bool isStarted();
        void Frame();
        
        void LoadScene(std::string filename);
        void CloseScene(std::string filename);
        
        std::vector<std::string> SceneFilenames();
        void AddVirtualFileSystem(std::string file_name);
        void AddVirtualFilePath(std::string path);
        
        void SetCurrentWorld(std::string world_name);
        
        void SetEnableFlightCam(bool enable);
    private:
        bool started = false;
        std::string viewport_name;
        bool g_flight_cam;
        std::string current_world_;
        
        void UpdateCamera(Camera *camera, RenderWorld *world);
        void OnWorldLoad(std::string world_name);
    };
    
    extern void ExitInteractiveMode();
    extern void WorldLoaded(std::string name);
}
#endif
