//
//  TINWorkshopController.m
//  SceneViewer
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINWorkshopController.h"
#import "TINVariableController.h"
#include <Engine/FileSystem.h>
#include <Engine/Render/RenderWorldManager.h>
@interface TINWorkshopController(){
    tin::Workshop *workshop;
}

@end
@implementation TINWorkshopController
static TINWorkshopController *workshopController = nullptr;
-(void)awakeFromNib{
    [super awakeFromNib];
}

-(void)dealloc{
    
}

-(void)startEngine{
    workshopController = self;
    workshop = new tin::Workshop();
    [self setApplication:workshop];
    [super startEngine];
    [self updateWorldSelector];
}

-(IBAction)displayRenderConfig:(id)sender{
    if (self.renderConfigController.window != nil){
        [self.renderConfigController.window makeKeyAndOrderFront:self];
        return;
    }
    NSWindowController *controller = [[NSWindowController alloc] initWithWindowNibName:@"RenderConfigWindow"];
    self.renderConfigController = controller;
    [self.renderConfigController showWindow:self];
    self.renderConfigController.window.delegate = self;
    [self.renderConfigController.window makeKeyAndOrderFront:self];
}

-(IBAction)displayHeirarchy:(id)sender{
    if (self.heirarchyWindowController.window != nil){
        [self.heirarchyWindowController.window makeKeyAndOrderFront:self];
        return;
    }
    NSWindowController *controller = [[NSWindowController alloc] initWithWindowNibName:@"HeirarchyWindow"];
    self.heirarchyWindowController = controller;
    [self.heirarchyWindowController showWindow:nil];
    self.heirarchyWindowController.window.delegate = self;
    [self.heirarchyWindowController.window makeKeyAndOrderFront:self];
}

-(IBAction)displayVariableWindow:(id)sender{
    if (self.variableWindowController.window != nil){
        [self.variableWindowController.window makeKeyAndOrderFront:self];
        return;
    }
    NSWindowController *controller = [[NSWindowController alloc] initWithWindowNibName:@"VariableWindow"];
    self.variableWindowController = controller;
    [self.variableWindowController showWindow:nil];
    self.variableWindowController.window.delegate = self;
    [self.variableWindowController.window makeKeyAndOrderFront:self];
}

-(IBAction)mouseControlButton:(id)sender{
    [self setFlightMode:!self.mouseActive];
}

-(void)setFlightMode:(BOOL)enabled{
    if (enabled){
        workshop->SetEnableFlightCam(true);
        self.mouseActive = true;
        [self lockMouse];
    } else {
        workshop->SetEnableFlightCam(false);
        self.mouseActive = false;
        [self unlockMouse];
    }
}

-(void)becameActive{
    // Override so that we don't lock mouse 
}

void tin::ExitInteractiveMode(){
    [workshopController setFlightMode:NO];
}

-(void)menuWillOpen:(NSMenu *)menu{
    if ([menu isEqual:self.closeMenu]){
        [self.closeMenu removeAllItems];
        auto names = tin::RenderWorldManager::SharedInstance()->RenderWorldNames();
        for (int i=0; i < names.size(); i++){
            NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:[NSString stringWithCString:names[i].c_str() encoding:NSUTF8StringEncoding] action:@selector(closeWorld:) keyEquivalent:@""];
            [item setTarget:self];
            [self.closeMenu addItem:item];
        }
    }
}

-(void)closeWorld:(id)sender{
    NSMenuItem *item = (NSMenuItem *)sender;
    std::string world_name = [item.title cStringUsingEncoding:NSUTF8StringEncoding];
    workshop->CloseScene(world_name);
    [self updateWorldSelector];
}

-(IBAction)openWorld:(id)sender{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    openPanel.allowedFileTypes = @[@"tinScene"];
    openPanel.allowsMultipleSelection = NO;
    openPanel.canChooseDirectories = NO;
    openPanel.canChooseFiles = YES;
    [openPanel runModal];
    
    // Should only be one, could easily do multiple opening
    for (NSURL *url in openPanel.URLs){
        std::string path = [[[url URLByDeletingLastPathComponent] path] cStringUsingEncoding:NSUTF8StringEncoding];
        tin::FileSystem::SharedInstance()->AddFileSystem(path, tin::VFSTypePath);
        std::string filename = [[url lastPathComponent] cStringUsingEncoding:NSUTF8StringEncoding];
        workshop->LoadScene(filename);
    }
    [self updateWorldSelector];
}

-(void)updateWorldSelector{
    [self.worldSelectorPopUp removeAllItems];
    auto names = tin::RenderWorldManager::SharedInstance()->RenderWorldNames();
    for (int i=0; i < names.size(); i++){
        [self.worldSelectorPopUp addItemWithTitle:[NSString stringWithCString:names[i].c_str() encoding:NSUTF8StringEncoding]];
    }
}

-(IBAction)selectWorld:(id)sender{
    
}

void WorldLoaded(std::string name){
    [workshopController updateWorldSelector];
}
@end
