//
//  TINWorldHeiarchy.h
//  Workshop
//
//  Created by Tobias Boogh on 5/6/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <string>
#include <vector>
#include <Engine/Render/RenderWorld.h>
typedef enum{
    Transform,
    Light,
    Mesh
} ObjectType;

@interface WorldItem : NSObject
@property (nonatomic) int index;
@property (nonatomic) int parentIndex;
@property (nonatomic) ObjectType type;
@property (nonatomic) int typeindex;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSValue *pointerValue;
@property (nonatomic, strong) NSMutableArray *childItems;
@end

@class TINNodeInfoController;
@interface TINWorldHeiarchy : NSObject <NSOutlineViewDataSource, NSOutlineViewDelegate>{
    std::vector<std::string> render_world_names_;
    tin::RenderWorld* current_world_;
}
@property (nonatomic, weak) IBOutlet TINNodeInfoController *infoController;
@property (nonatomic, weak) IBOutlet NSPopUpButton *renderWorldPopUpButton;
@property (nonatomic, weak) IBOutlet NSOutlineView *heirarchyView;
@property (nonatomic, strong) WorldItem* rootItem;
@property (nonatomic, strong) NSMutableArray* WorldItemArray;
-(IBAction)worldSelectorChanged:(id)sender;
@end
