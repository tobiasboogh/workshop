//
//  TINVariableController.m
//  Workshop
//
//  Created by Tobias Boogh on 5/11/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINVariableController.h"
#include <Engine/VariableSystem.h>
#include <vector>
@interface TINVariableController(){
    std::vector<tin::Variable *> variables;
}

@end

@implementation TINVariableController
-(void)awakeFromNib{
    variables = tin::VariableSystem::SharedInstance()->GetAllVariables();
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    return variables.size();
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    tin::Variable *var = variables[row];
    if ([tableColumn.identifier isEqualTo:@"name"]){
        return [NSString stringWithCString:var->name().c_str() encoding:NSUTF8StringEncoding];
    }
    switch(var->type()){
        case tin::kVariableTypeBool:
            return @(var->asBool());
            break;
        case tin::kVariableTypeInt:
            return @(var->asInt());
            break;
        case tin::kVariableTypeFloat:
            return @(var->asFloat());
            break;
        case tin::kVariableTypeDouble:
            return @(var->asDouble());
            break;
        case tin::kVariableTypeString:
            return [NSString stringWithCString:var->asString().c_str() encoding:NSUTF8StringEncoding];
            break;
    }
    return nil;
}

-(void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    tin::Variable *var = variables[row];
    switch(var->type()){
        case tin::kVariableTypeBool:
            var->SetValue([object boolValue]);
            break;
        case tin::kVariableTypeInt:
            var->SetValue([object intValue]);
            break;
        case tin::kVariableTypeFloat:
            var->SetValue([object floatValue]);
            break;
        case tin::kVariableTypeDouble:
            var->SetValue([object doubleValue]);
            break;
        case tin::kVariableTypeString:
            var->SetValue([object cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
    }
}
@end
