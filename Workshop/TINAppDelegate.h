//
//  TINAppDelegate.h
//  Workshop
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include "TINWorkshopController.h"
@interface TINAppDelegate : NSObject <NSApplicationDelegate>
@property (weak) IBOutlet TINWorkshopController *engineObject;
@property (assign) IBOutlet NSWindow *window;

@end
