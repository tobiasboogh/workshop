//
//  TINRenderConfigController.h
//  SceneViewer
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#include <Engine/EngineCommon.hpp>

@interface TINRenderConfigController : NSObject <NSTableViewDataSource, NSTableViewDelegate, NSTabViewDelegate, NSComboBoxCellDataSource>

@property (nonatomic, weak) IBOutlet NSTableView *viewportTableView;
@property (nonatomic, weak) IBOutlet NSTableView *layerTableView;
@property (nonatomic, weak) IBOutlet NSTableView *layerRendertargetsTableView;
@property (nonatomic, weak) IBOutlet NSTableView *layerInputsTableView;
@property (nonatomic, weak) IBOutlet NSTableView *globalRenderTargetsTableView;
@property (nonatomic, weak) IBOutlet NSButton *clearCheckbox;
@property (weak) IBOutlet NSButton *writedepthCheckbox;
@property (weak) IBOutlet NSButton *writecolorCheckbox;
@property (weak) IBOutlet NSPopUpButton *modifierPopupButton;
@property (weak) IBOutlet NSPopUpButton *depthTargetPopupButton;

-(IBAction)addViewport:(id)sender;
-(IBAction)removeViewport:(id)sender;

-(IBAction)addLayer:(id)sender;
-(IBAction)removeLayer:(id)sender;
-(IBAction)moveLayerUp:(id)sender;
-(IBAction)moveLayerDown:(id)sender;

-(IBAction)addRenderTarget:(id)sender;
-(IBAction)removeRenderTarget:(id)sender;
-(IBAction)moveRenderTargetUp:(id)sender;
-(IBAction)moveRenderTargetDown:(id)sender;

-(IBAction)addGlobalRenderTarget:(id)sender;
-(IBAction)removeGlobalRenderTarget:(id)sender;

-(IBAction)checkboxChanged:(id)sender;
-(IBAction)popUpChanged:(id)sender;

-(IBAction)addInput:(id)sender;
-(IBAction)removeInput:(id)sender;
-(IBAction)moveInputUp:(id)sender;
-(IBAction)moveInputDown:(id)sender;

-(IBAction)updateRenderConfig:(id)sender;

-(IBAction)saveRenderConfig:(id)sender;

-(IBAction)openRenderConifg:(id)sender;
@end
