//
//  TINNodeInfoController.m
//  Workshop
//
//  Created by Tobias Boogh on 5/7/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//
#import <glm/gtx/string_cast.hpp>
#import "TINNodeInfoController.h"
#import "Editor/EditorClasses.h"
@interface TINSection : NSObject {
    tin::EditorDefBase *definition;
}
@property (nonatomic, strong) NSMutableArray *children;
@property (nonatomic, strong) TINSection *parentSection;
-(void)setDef:(tin::EditorDefBase *)def;
-(tin::EditorDefBase *)getDef;
@end

@implementation TINSection

-(id)init{
    self = [super init];
    if (self){
        _children = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)setDef:(tin::EditorDefBase *)def{
    definition = def;
}

-(tin::EditorDefBase *)getDef{
    return definition;
}
@end

@implementation TINNodeInfoController
-(void)displayInfoForWorldItem:(WorldItem *)item{
    self.currentItem = item;
    TINSection *section = nil;
    if (item.type == Light){
        tin::Light *light = static_cast<tin::Light *>([item.pointerValue pointerValue]);
        auto light_def = tin::EditorDefBase::LightDef("Light", light);
        section = [self sectionForEditorDef:light_def];
    } else if (item.type == Transform){
        tin::Transform *transform = static_cast<tin::Transform *>([item.pointerValue pointerValue]);
        auto trans_def = tin::EditorDefBase::TransformDef("Transform", transform);
        
        section = [self sectionForEditorDef:trans_def];
    }
    self.rootSection = section;
    [self.outlineView reloadData];
}

-(TINSection *)sectionForEditorDef:(tin::EditorDefBase *)def{
    TINSection *section = [[TINSection alloc] init];
    [section setDef:def];
    for (int i=0; i < def->NumProperties(); i++){
        TINSection *subSection = [self sectionForEditorDef:def->Property(i)];
        [section.children addObject:subSection];
    }
    return section;
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item{
    if (item == nil){
        return self.rootSection.children.count;
    }
    return [[item children] count];
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item{
    TINSection *section = nil;
    if (item == nil){
        section = self.rootSection;
    } else {
        section = (TINSection *)item;
    }
    return [section.children objectAtIndex:index];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item{
    TINSection *section = nil;
    if (item == nil){
        section = self.rootSection;
    } else {
        section = (TINSection *)item;
    }
    return (section.children.count > 0);
}

-(BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item{
    return [self outlineView:outlineView isItemExpandable:item];
}

//-(NSCell *)outlineView:(NSOutlineView *)outlineView dataCellForTableColumn:(NSTableColumn *)tableColumn item:(id)item{
//    TINSection *section = (TINSection *)item;
//    tin::EditorDefBase *baseDef = static_cast<tin::EditorDefBase *>([section.value pointerValue]);
//    if (baseDef->type() == tin::kValueClass){
//        return NO;
//    }
//    return YES;
//}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item{
    TINSection *section = (TINSection *)item;
    tin::EditorDefBase *baseDef = [section getDef];
    if ([tableColumn.identifier isEqualToString:@"name"]){
        return [NSString stringWithCString:baseDef->name().c_str() encoding:NSUTF8StringEncoding];
    }else if ([tableColumn.identifier isEqualToString:@"value"]){
        switch (baseDef->type()){
            case tin::kValueBool:{
                auto def = static_cast<tin::EditorDef<bool *> *>(baseDef);
                return @(*def->value());
            } break;
            case tin::kValueInt:{
                auto def = static_cast<tin::EditorDef<int *> *>(baseDef);
                return @(*def->value());
            } break;
            case tin::kValueFloat:{
                tin::EditorDef<float *> *def = static_cast<tin::EditorDef<float *> *>(baseDef);
                return [NSNumber numberWithFloat:*def->value()];
            } break;
            case tin::kValueVec2:{
                auto def = static_cast<tin::EditorDef<glm::vec2 *> *>(baseDef);
                std::string string = glm::to_string(*def->value());
                return [NSString stringWithCString:string.c_str() encoding:NSUTF8StringEncoding];
            } break;
            case tin::kValueVec3:{
                auto def = static_cast<tin::EditorDef<glm::vec3 *> *>(baseDef);
                std::string string = glm::to_string(*def->value());
                return [NSString stringWithCString:string.c_str() encoding:NSUTF8StringEncoding];
            } break;
            case tin::kValueVec4:{
                auto def = static_cast<tin::EditorDef<glm::vec4 *> *>(baseDef);
                std::string string = glm::to_string(*def->value());
                return [NSString stringWithCString:string.c_str() encoding:NSUTF8StringEncoding];
            } break;
            case tin::kValueQuat:{
                auto def = static_cast<tin::EditorDef<glm::quat *> *>(baseDef);
                glm::vec4 v = glm::vec4(def->value()->x, def->value()->y, def->value()->z, def->value()->w);
                return [NSString stringWithCString:glm::to_string(v).c_str() encoding:NSUTF8StringEncoding];
            } break;
            case tin::kValueMat3x3:
                break;
            case tin::kValueMat4x4:
                break;
            case tin::kValueClass:
                return nil;
                break;
        }
    }
    return nil;
}

-(void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item{
    TINSection *section = (TINSection *)item;
    tin::EditorDefBase *baseDef = [section getDef];
    switch (baseDef->type()){
        case tin::kValueBool:{
            auto def = static_cast<tin::EditorDef<bool *> *>(baseDef);
            *def->value() = [object boolValue];
        } break;
        case tin::kValueInt:{
            auto def = static_cast<tin::EditorDef<int *> *>(baseDef);
            def->value([object intValue]);
        } break;
        case tin::kValueFloat:{
            auto def = static_cast<tin::EditorDef<float *> *>(baseDef);
            def->value([object floatValue]);
        } break;
        case tin::kValueVec2:{
        } break;
        case tin::kValueVec3:{
        } break;
        case tin::kValueVec4:{
        } break;
        case tin::kValueQuat:{
        } break;
        case tin::kValueMat3x3:
            break;
        case tin::kValueMat4x4:
            break;
        case tin::kValueClass:
            
            break;
    }
}
@end
