//
//  TINWorldHeiarchy.m
//  Workshop
//
//  Created by Tobias Boogh on 5/6/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINWorldHeiarchy.h"
#include "TINNodeInfoController.h"
#include <Engine/Scenegraph/Light.h>
#include <Engine/Render/RenderWorldManager.cpp>
@implementation WorldItem
-(id)init{
    self = [super init];
    if (self){
        self.childItems = [[NSMutableArray alloc] init];
    }
    return self;
}
@end

@implementation TINWorldHeiarchy
-(void)awakeFromNib{
    render_world_names_ = tin::RenderWorldManager::SharedInstance()->RenderWorldNames();
    [self refreshControls];
    [self loadWorldForItem:[self.renderWorldPopUpButton itemTitleAtIndex:0]];
}

-(void)refreshControls{
    [self.renderWorldPopUpButton removeAllItems];
    for (auto name : render_world_names_){
        [self.renderWorldPopUpButton addItemWithTitle:[NSString stringWithFormat:@"%s", name.c_str()]];
    }
}

-(IBAction)worldSelectorChanged:(id)sender{
    [self loadWorldForItem:[sender titleOfSelectedItem]];
}

-(void)loadWorldForItem:(NSString *)item{
    current_world_ = tin::RenderWorldManager::SharedInstance()->GetWorld([item cStringUsingEncoding:NSUTF8StringEncoding]);
    // Wrap renderworld heirarchy in Cocoa Object
    self.rootItem = [[WorldItem alloc] init];
    self.rootItem.index = -1;
    self.WorldItemArray = [[NSMutableArray alloc] init];
    [self prepareItemsWithParent:self.rootItem];
    
    for (int i=0; i < current_world_->NumLights(); i++){
        tin::Light *light = current_world_->GetLight(i);
        WorldItem *lightItem = [[WorldItem alloc] init];
        lightItem.parentIndex = light->TransformId();
        lightItem.name = [NSString stringWithFormat:@"Light_%d", i];
        lightItem.type = Light;
        lightItem.pointerValue = [NSValue valueWithPointer:static_cast<void *>(light)];

        [[[self.WorldItemArray objectAtIndex:lightItem.parentIndex] childItems] addObject:lightItem];
    }
    
    [self.heirarchyView reloadData];
}

-(void)prepareItemsWithParent:(WorldItem *)item{
    for (int i=0; i < current_world_->NumTransforms(); i++){
        if (current_world_->GetTransform(i).parent() == item.index){
            WorldItem *childitem = [[WorldItem alloc] init];
            childitem.index = i;
            childitem.name = [NSString stringWithFormat:@"Transform_%d", i];
            childitem.parentIndex = item.index;
            childitem.pointerValue = [NSValue valueWithPointer:current_world_->GetTransformPtr(i)];
            [item.childItems addObject:childitem];
            [[self WorldItemArray] addObject:childitem];
            [self prepareItemsWithParent:childitem];
        }
    }
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item{
    if (item == nil){
        return self.rootItem.childItems.count;
    }
    return [[item childItems] count];
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item{
    WorldItem *tItem = (WorldItem *)item;
    if (item == nil){
        tItem = self.rootItem;
    }
    return [tItem.childItems objectAtIndex:index];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item{
    WorldItem *tItem = (WorldItem *)item;
    return !(tItem.childItems.count == 0);
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item{
    WorldItem *tItem = (WorldItem *)item;
    return tItem.name;
}

-(BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item{
    WorldItem *tItem = (WorldItem *)item;
    [self.infoController displayInfoForWorldItem:tItem];
    return YES;
}
@end
