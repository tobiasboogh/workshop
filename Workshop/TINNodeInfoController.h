//
//  TINNodeInfoController.h
//  Workshop
//
//  Created by Tobias Boogh on 5/7/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TINWorldHeiarchy.h"
@class TINSection;
@interface TINNodeInfoController : NSObject <NSOutlineViewDataSource, NSOutlineViewDelegate>
@property (nonatomic, weak) IBOutlet NSOutlineView *outlineView;
@property (nonatomic, strong) WorldItem *currentItem;
@property (nonatomic, strong) TINSection *rootSection;
-(void)displayInfoForWorldItem:(WorldItem *)item;

@end
