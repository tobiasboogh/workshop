//
//  TINAppDelegate.m
//  Workshop
//
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINAppDelegate.h"

@implementation TINAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}


-(void)applicationWillTerminate:(NSNotification *)notification{
    [self.engineObject stopEngine];
}

-(void)applicationDidBecomeActive:(NSNotification *)notification{
    [self.engineObject becameActive];
}

-(void)applicationDidResignActive:(NSNotification *)notification{
    [self.engineObject didResignActive];
}
@end
