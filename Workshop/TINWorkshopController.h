//
//  TINWorkshopController.h
//  SceneViewer
///Users/tobiasboogh/Projects/tincan/Engine/Sceneviewer/TestFiles/Scenes
//  Created by Tobias Boogh on 5/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINEngineController.h"
#include "Workshop.h"
@interface TINWorkshopController : TINEngineController <NSToolbarDelegate, NSWindowDelegate, NSMenuDelegate>
@property (nonatomic, strong) NSWindowController *renderConfigController;
@property (nonatomic, strong) NSWindowController *heirarchyWindowController;
@property (nonatomic, strong) NSWindowController *variableWindowController;
@property (nonatomic, weak) IBOutlet NSMenu *closeMenu;
@property (nonatomic, weak) IBOutlet NSPopUpButton *worldSelectorPopUp;
@property (nonatomic) bool mouseActive;
-(IBAction)displayRenderConfig:(id)sender;
-(IBAction)displayHeirarchy:(id)sender;
-(IBAction)mouseControlButton:(id)sender;
-(IBAction)displayVariableWindow:(id)sender;
-(IBAction)openWorld:(id)sender;
-(IBAction)selectWorld:(id)sender;
@end
