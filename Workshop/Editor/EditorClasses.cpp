//
//  EditorClasses.cpp
//  Workshop
//
//  Created by Tobias Boogh on 5/9/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "EditorClasses.h"

namespace tin {
    void EditorDefBase::AddDef(std::string name, float *f){
        auto def = new tin::EditorDef<float *>(name, f, tin::kValueFloat);
        this->AddProperty(def);
    }
    
    void EditorDefBase::AddDef(std::string name, glm::vec3 *vec3){
        auto def = new tin::EditorDef<glm::vec3 *>(name, vec3, tin::kValueVec3);
        def->AddDef("X", &vec3->x);
        def->AddDef("Y", &vec3->y);
        def->AddDef("Z", &vec3->z);
        this->AddProperty(def);
    }
    
    void EditorDefBase::AddDef(std::string name, glm::vec4 *vec4){
        auto def = new tin::EditorDef<glm::vec4 *>(name, vec4, tin::kValueVec4);
        if (name == "Color"){
            def->AddDef("R", &vec4->x);
            def->AddDef("G", &vec4->y);
            def->AddDef("B", &vec4->z);
            def->AddDef("A", &vec4->w);
        } else {
            def->AddDef("X", &vec4->x);
            def->AddDef("Y", &vec4->y);
            def->AddDef("Z", &vec4->z);
            def->AddDef("W", &vec4->w);
        }
        this->AddProperty(def);
    }
    
    void EditorDefBase::AddDef(std::string name, glm::quat *quat){
        auto def = new tin::EditorDef<glm::quat *>(name, quat, tin::kValueQuat);
        def->AddDef("X", &quat->x);
        def->AddDef("Y", &quat->y);
        def->AddDef("Z", &quat->z);
        def->AddDef("W", &quat->w);
        this->AddProperty(def);
    }
    
    EditorDefBase*  EditorDefBase::LightDef(std::string name, tin::Light *light){
        tin::EditorDef<tin::Light *> *light_def = new tin::EditorDef<tin::Light *>("Light", light, tin::kValueClass);
        light_def->AddDef("Intensity", &light->intensity_);
        light_def->AddDef("Color", &light->color_);
        return light_def;
    }
    
    EditorDefBase* EditorDefBase::TransformDef(std::string name, tin::Transform *transform){
        tin::EditorDef<tin::Transform *> *trans_def = new tin::EditorDef<tin::Transform *>("Transform", transform, tin::kValueClass);
        trans_def->AddDef("Position", &transform->position_);
        trans_def->AddDef("Rotation", &transform->rotation_);
        trans_def->AddDef("Scale", &transform->scale_);
        return trans_def;
    }
}