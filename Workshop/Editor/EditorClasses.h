//
//  tin.h
//  Workshop
//
//  Created by Tobias Boogh on 5/9/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _tin_H_
#define _tin_H_
#include <vector>
#include <string>
#include <glm/glm.hpp>

#include <Engine/Scenegraph/Light.h>
#include <Engine/Scenegraph/Camera.h>
#include <Engine/Scenegraph/Transform.h>

namespace tin{
    typedef enum{
        kValueBool,
        kValueInt,
        kValueFloat,
        kValueVec2,
        kValueVec3,
        kValueVec4,
        kValueQuat,
        kValueMat3x3,
        kValueMat4x4,
        kValueClass
    } ValueType;
    
        class EditorDefBase{
            public:
                virtual ~EditorDefBase() { };
                std::string name() { return name_; }
                void AddProperty(EditorDefBase *editorDef){
                    properties_.push_back(editorDef);
                }
            
                int64_t NumProperties(){
                    return properties_.size();
                }
            
                EditorDefBase* Property(int index){
                    return properties_[index];
                }
            
                ValueType type(){
                    return type_;
                }
            
                void AddDef(std::string name, float *f);
                void AddDef(std::string name, glm::vec3 *vec3);
                void AddDef(std::string name, glm::vec4 *vec4);
                void AddDef(std::string name, glm::quat *quat);
            
                static EditorDefBase* LightDef(std::string name, Light *light);
                static EditorDefBase* TransformDef(std::string name, Transform *transform);
            protected:
                std::vector<EditorDefBase *> properties_;
                std::string name_;
                ValueType type_;
            
                friend class Light;
        };
    
        template<typename T>
        class EditorDef : public EditorDefBase {
            public:
                EditorDef(std::string name, T value, ValueType type){
                    name_ = name;
                    value_ = value;
                    type_ = type;
                }
            
                T value() { return value_; };
            
                template<typename U>
                void value(U value) {
                    if (typeid(*value_) != typeid(value)){
                        return;
                    }
                    *value_ = value;
                };
            private:
                T value_;
        };
} // tin
#endif
