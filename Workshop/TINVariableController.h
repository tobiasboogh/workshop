//
//  TINVariableController.h
//  Workshop
//
//  Created by Tobias Boogh on 5/11/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TINVariableController : NSObject <NSTableViewDataSource, NSTableViewDelegate>
@property (nonatomic, weak) IBOutlet NSTableView *tableView;
@end
